const checkEmail = (email) => {
	
     let Regex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;

    if (email == null) {
        return 'ERROR : harus ada isinya'
    }
    if (typeof email != 'string') {
        return 'ERROR : data harus string'
    }
    return email.match(Regex) ? 'VALID' : 'INVALID';

       

      
}
console.log(checkEmail('apranata@binar.co.id'));
console.log(checkEmail('apranata@binar.com'));
console.log(checkEmail('apranata@binar'));
console.log(checkEmail('apranata'));
console.log(checkEmail(3322));
console.log(checkEmail());


